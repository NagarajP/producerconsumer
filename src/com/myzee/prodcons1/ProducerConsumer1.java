package com.myzee.prodcons1;

import java.util.LinkedList;

public class ProducerConsumer1 {
	public static int capacity = 5;
	public static int item = 0;
	static LinkedList<Integer> l = new LinkedList<>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Producer p = new Producer(l);
		Consumer c = new Consumer(l);
		
		p.start();
		c.start();
		
	}

}

class Producer extends Thread {
	LinkedList<Integer> l;
	Producer(LinkedList<Integer> l) {
		this.l = l;
	}
	public void run() {
		while(true) {
			synchronized(l) {
			if(l.size() == ProducerConsumer1.capacity) {
				System.out.println("waiting for consumer");
//				l.notify();
					try {
						l.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
				System.out.println(Thread.currentThread().getName() + " Produced - " + ProducerConsumer1.item);
				l.add(ProducerConsumer1.item++);
				l.notify();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
		}
		}
	}
}

class Consumer extends Thread {
	LinkedList<Integer> l;
	Consumer(LinkedList<Integer> l) {
		this.l = l;
	}
	
	public void run() {
		while(true) {
			synchronized(l) {
			if(l.size() == 0) {
				System.out.println("waiting for producer for data");
				try {
					l.wait();
//					l.notify();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread().getName()+ " Consumed - " + l.remove());
				l.notify();
		}
		}
	}
}
