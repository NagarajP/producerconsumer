package com.myzee.prodcons;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProdConsUsingBlockingQueue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BlockingQueue<Integer> b = new ArrayBlockingQueue<>(10);
		Producer1 p = new Producer1(b);
		Consumer1 c = new Consumer1(b);
		
		p.start();
		c.start();
	}

}

class Producer1 extends Thread {
	BlockingQueue<Integer> b;
	public Producer1(BlockingQueue<Integer> b) {
		this.b = b;
	}
	public void run() {
		for(int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
				System.out.println(Thread.currentThread().getName() + " added item - " + i);
				b.put(i);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Queue is full");
		b.add(-1);
	}
}

class Consumer1 extends Thread {
	
	BlockingQueue<Integer> b;
	public Consumer1(BlockingQueue<Integer> b) {
		this.b = b;
	}
	
	public void run() {
		try {
			Integer t;
			while((t = b.take()) != -1) {
				Thread.sleep(1000);
				System.out.println(Thread.currentThread().getName() + " item consumed " + t);
			}
			System.out.println(Thread.currentThread().getName() + " item consumed " + t);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
