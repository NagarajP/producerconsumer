package com.myzee.prodcons;

import java.util.ArrayList;
import java.util.List;

public class ProdConsEx1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> sharedList = new ArrayList<Integer>();
		Producer p = new Producer(sharedList);
		Consumer c = new Consumer(sharedList);
		
		p.start();
		c.start();
	}

}

class Producer extends Thread {
	List<Integer> sharedList = null;
	int i = 0;
	final int MAX_SIZE = 5;

	Producer(List<Integer> sharedList) {
		this.sharedList = sharedList;
	}
	
	public void run() {
		while(true) {
		try {
			produce(i++);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}

	public void produce(int i) throws InterruptedException {
		synchronized (sharedList) {
			while (sharedList.size() == MAX_SIZE) {
				System.out.println("list is full, waiting for consumer to consume");
				sharedList.wait();
			}
		}

		synchronized (sharedList) {
			System.out.println("Producer produced the element - " + i);
			sharedList.add(i);
			Thread.sleep(1000);
//			sharedList.wait();
			sharedList.notify();
		}
	}
}

class Consumer extends Thread {
	List<Integer> sharedList = null;

	Consumer(List<Integer> sharedList) {
		this.sharedList = sharedList;
	}

	public void run() {
		while(true) {
		try {
			consume();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}

	public void consume() throws InterruptedException {
		synchronized (sharedList) {
			while (sharedList.isEmpty()) {
				System.out.println("List is empty, waiting for producer to produce");
				sharedList.wait();
			}
		}
		synchronized (sharedList) {
			Thread.sleep(1000);
			System.out.println("element consumed - " + sharedList.remove(0));
			sharedList.notify();
		}

	}
}